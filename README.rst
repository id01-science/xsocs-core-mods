X-SOCS
======

The X-ray Strain Orientation Calculation Software (X-SOCS) is a user-friendly software,
developed for automatic analysis of 5D sets of data recorded during continuous mapping measurements.
X-SOCS aims at retrieving strain and tilt maps of nanostructures, films, surfaces or even embedded structures.

`Documentation <http://kmap.gitlab-pages.esrf.fr/xsocs>`_

Installation
------------

X-SOCS runs on Linux and Windows with `Python <https://www.python.org/>`_ >=3.7.

See `How to install XSocs <http://kmap.gitlab-pages.esrf.fr/xsocs/install.html>`_ for details and dependencies.

Using XSOCS
------------

Once installed, you can run X-SOCS from the console with either::

    xsocs

Or::

    python -m xsocs

See `Using X-Socs documentation <http://kmap.gitlab-pages.esrf.fr/xsocs/using.html>`_ and
the `video tutorials <http://kmap.gitlab-pages.esrf.fr/xsocs/tutorials.html>`_.

License
-------

The source code of X-SOCS is licensed under the `MIT license <https://gitlab.esrf.fr/kmap/xsocs/blob/main/LICENSE>`_.
